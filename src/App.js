import "./App.css";
import About from "./Components/About/About";
import Contact from "./Components/ContactMe/Contact";
import Footer from "./Components/Footer/Footer";
import Home from "./Components/Home/Home";
import Project from "./Components/Project/Project";
import Skills from "./Components/Skills/Skills";

function App() {
  return (
    <div className="App">
      <Home />
      <About />
      <Skills />
      <Project />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
