import React, { useState } from "react";
import "./Home.css";
import logo from "../../assets/img/logo.png";
import homeBg from "../../assets/img/Home-bg.png";

export default function Home() {
  ////fixed Header///////
  window.addEventListener("scroll", function () {
    const header = this.document.querySelector(".header");
    header.classList.toggle("active", window.scrollY > 0);
  });
  //Toggle menu ////
  const [show, setShow] = useState();

  return (
    <div className="home" id="Home">
      <div className="home-bg">
        <div className="header d-flex align-items-center">
          <div className="logo">
            <img src={logo} alt="logo" />
          </div>
          <div className="navigation pxy-30">
            <ul className="navbar d-flex">
              <a href="#Home">
                <li className="nav-items mx-15">Home</li>
              </a>
              <a href="#About">
                <li className="nav-items mx-15">About</li>
              </a>
              <a href="#Skills">
                <li className="nav-items mx-15">Skill's</li>
              </a>
              <a href="#Projects">
                <li className="nav-items mx-15">Project's</li>
              </a>
              <a href="#Contact">
                <li className="nav-items mx-15">Contact Me</li>
              </a>
            </ul>
          </div>
          {/* toggle menu */}
          <div className="toggle-menu">
            <svg
              onClick={() => setShow(!show)}
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              class="bi bi-justify white pointer"
              viewBox="0 0 16 16"
            >
              <path
                fill-rule="evenodd"
                d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
              />
            </svg>
          </div>
          {show ? (
            <div className="sideNavbar">
              <ul className="sidebar d-flex">
                <li className="sideNavbar">
                  <a href="#Home">Home</a>
                </li>
                <li className="sideNavbar">
                  <a href="#About">About</a>
                </li>
                <li className="sideNavbar">
                  <a href="#Skills">Skills</a>
                </li>
                <li className="sideNavbar">
                  <a href="#Projects">Projects</a>
                </li>
                <li className="sideNavbar">
                  <a href="#Contact">Contact Me</a>
                </li>
              </ul>
            </div>
          ) : null}
        </div>
        {/* ///////////Home Content/////////////*/}
        <div className="container">
          <div className="row">
            <div className="col-2">
              <div className="home-content">
                <div className="home-meta">
                  <h1 className="home-text pz-10">
                    WELCOME TO MY WORLD{" "}
                    <i style={{ color: "red" }} className="fas fa-heart"></i>
                  </h1>
                  <h2 className="home-text pz-10">Hi I'm Morteza Dasomi</h2>
                  <h3 className="home-text sweet pz-10">FЯOŊT £ŊD D£V£ŁOΡ£Я</h3>
                </div>
              </div>
            </div>
            <div className="col-3">
              <img className="homeImg" src={homeBg} alt="aboutImg" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
