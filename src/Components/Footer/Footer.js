import React from "react";
import "./Footer.css";
import footerImg from "../../assets/img/logo.png";
export default function Footer() {
  return (
    <div className="footer d-flex flex-center ">
      <img className="footer-img" src={footerImg} alt="footerImg" />
      <span className="thank">Thank You For Visiting</span>
    </div>
  );
}
