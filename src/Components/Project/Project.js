import React from "react";
import "./Project.css";
// import "./count";
import vanda from "../../assets/img/vanda.png";
import zns from "../../assets/img/zns.png";
import pishro from "../../assets/img/pishro.png";
import routeCoin from "../../assets/img/route-coin.png";
// import marvel from "../../assets/img/marvel.png";
import mrFood from "../../assets/img/mr-food.png";
import lorem from "../../assets/img/lorem.png";
// import XO from "../../assets/img/screencapture-tic-toc-xo-vercel-app-2022-01-15-02_28_19.png";
// import musicPlayer from "../../assets/img/screencapture-music-player-me-vercel-app-2022-01-20-08_00_45.png";

export default function Project() {
  const projects = [
    {
      link: "https://zns-five.vercel.app/",
      img: zns,
    },
    {
      link: "http://routecoin.io",
      img: routeCoin,
    },
    {
      link: "https://vandasmartworld.com",
      img: vanda,
    },
    {
      link: "https://portofolio-ashen-tau.vercel.app/",
      img: lorem,
    },
    // {
    //   link: "https://pishropardakht-new.vercel.app/",
    //   img: pishro,
    // },
    // {
    //   link: "https://vps.marveltrade.org",
    //   img: marvel,
    // },
    {
      link: "https://mr-food.vercel.app/",
      img: mrFood,
    },
    // {
    //   link: "https://tic-toc-xo.vercel.app/",
    //   img: XO,
    // },
    // {
    //   link: "https://music-player-me.vercel.app/",
    //   img: musicPlayer,
    // },
  ];
  return (
    <div className="projects component-space" id="Projects">
      <div className="heading">
        <h1 className="heading">My Latest Project</h1>
      </div>
      {/* <section id="counter">
        <section className="counter-section mt">
          <div className="container">
            <div className="row">
              <div className="counter">
                <div>
                  <h2>
                    <span id="count1"></span>
                  </h2>
                  <p>EMPLOYER'S</p>
                </div>
                <div className="">
                  <h2>
                    <span id="count2"></span>
                  </h2>
                  <p>PROJECT</p>
                </div>
                <div className="">
                  <h2>
                    <span id="count3"></span>
                  </h2>
                  <p>HOURS'S</p>
                </div>
                <div className="">
                  <h2>
                    <span id="count4"></span>
                  </h2>
                  <p>MINUTE'S</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section> */}
      <div className="container">
        <div className="row mt-5">
          {projects.map((item, index) => (
            <div key={index} className="col-3">
              {/* <div className="project-box pointer relative">
                <div className="project-box-img pointer relative">
                  <div className="project-img-box">
                    <img src={item.img} alt="project" />
                  </div>
                  <div className="mask-effect m-grad"></div>
                </div>
                <div className="project-meta absolute flex-center">
                  <a href={item.link} className="project-btn" target={"_blank"}>
                    Visit Project
                  </a>
                </div>
              </div> */}
              <div className="project">
                <img src={item.img} alt={item.link} />
                <div className="content">
                  <a href={item.link} className="project-btn" target={"_blank"}>
                    Visit Project
                  </a>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
