import React from "react";
import "./Skills.css";
import html from "../../assets/icon/download.svg";
import css from "../../assets/icon/Css.png";
import javaScript from "../../assets/icon/1024px-Unofficial_JavaScript_logo_2.svg.png";
import sass from "../../assets/icon/1280px-Sass_Logo_Color.svg.png";
import bootStrap from "../../assets/icon/Bootstrap_(front-end_framework)-Logo.wine.png";
import iReact from "../../assets/icon/react.928f956d.svg";
import responsive from "../../assets/icon/responsive design.0fd2b828.png";
import fa from "../../assets/icon/fa.png";
import en from "../../assets/icon/en.png";
import ar from "../../assets/icon/ar.png";
import git from "../../assets/icon/gitlab.c2446ea6.png";
import cleanCode from "../../assets/icon/clean code.ec2bc547.png";

export default function Skills() {
  return (
    <div className="skills component-space" id="Skills">
      <div className="heading">
        <h1 className="heading">My Professional Skill's</h1>
      </div>
      <div className="container">
        <div className="row mt-5">
          <div className="col-3 ">
            <div className="skills-box m-grad pointer">
              <div className="skills-img">
                <img src={iReact} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">REACT.JS</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={html} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">HTML 5</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={css} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">CSS</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={javaScript} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">JAVA SCRIPT</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={bootStrap} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">BOOTSTRAP 5</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={sass} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">SASS</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={git} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">GIT</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={responsive} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">RESPONSIVE</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <img src={cleanCode} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">CLEAN CODE</h1>
              </div>
            </div>
          </div>

          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <h1>Language</h1>
                <img src={fa} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">PERSIAN</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <h1>Language</h1>

                <img src={en} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">ENGLISH</h1>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="skills-box pointer m-grad">
              <div className="skills-img">
                <h1>Language</h1>

                <img src={ar} alt="react-i" />
              </div>
              <div className="skills-meta">
                <h1 className="skills-header">ARABIC</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
