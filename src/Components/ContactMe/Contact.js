import React from "react";
import "./Contact.css";

export default function Contact() {
  return (
    <div className="contact component-space mt" id="Contact">
      <div className="row">
        <div className="contact-box">
          <div className="contact-meta ">
            <h1 className="hire-header">Hire Me!</h1>
            <p className="hire-text white">I Am Available All The Time</p>
            <p className="hire-text white">
              <strong>CONTACT ME:</strong>
            </p>
          </div>
          <div className="wrapper-btn">
            <button className="bg1">
              <a
                href="https://www.instagram.com/morteza_desomi"
                target={"_blank"}
              >
                <i className="fab fa-instagram"></i>
              </a>
            </button>
            <button className="bg2">
              <a href="https://t.me/Morteza1378" target={"_blank"}>
                <i className="fab fa-telegram"></i>
              </a>
            </button>
            <button className="bg3">
              <a
                href="http://www.linkedin.com/in/morteza-dasomi-46459220a"
                target={"_blank"}
              >
                <i className="fab fa-linkedin"></i>
              </a>
            </button>
            <button className="bg4">
              <a href="https://wa.me/+989211437264" target={"_blank"}>
                <i className="fab fa-whatsapp"></i>
              </a>
            </button>
            <button className="bg5">
              <a href="mailto:dasomimorteza@gmail.com" target={"_blank"}>
                <i className="far fa-envelope"></i>
              </a>
            </button>
            <button className="bg6">
              <a href="tel:+989362862425">
                <i className="fas fa-phone"></i>
              </a>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
