import React from "react";
import "./About.css";
import aboutImg from "../../assets/img/about-bg.png";
import resume from "../../assets/file/resume.pdf";

export default function About() {
  window.addEventListener("scroll", function () {
    const upToTop = this.document.querySelector("a.bottom-to-top");
    upToTop.classList.toggle("active", window.scrollY > 0);
  });
  return (
    <div className="about component-space" id="About">
      <div className="container">
        <div className="row">
          <div className="col-2">
            <img className="aboutImg" src={aboutImg} alt="aboutImg" />
          </div>
          <div className="col-2 mt about-me-text">
            <h1 className="about-heading">About Me</h1>
            <div className="about-meta">
              <p className="about-text p-color">
                Hello Friend I'm Morteza Dasomi and i born in 10/03/2000.
              </p>
              <p className="about-text p-color">I am FRONT-END-DEVELOPER.</p>
              <p className="about-text p-color">
                Creative and intelligent,loves challenge and problem solving.
              </p>
              <p className="about-text p-color">
                This page is created to Introduce me and show My Skill I made
                this page to gain your attention and trust.
              </p>
              <p className="about-text p-color">I hope you enjoyed.</p>
              <p className="about-text p-color">
                Thank You For Giving Me Your Time.
              </p>
              {/* <div className="about-button d-flex align-items-center">
                <a href={resume} download>
                  <button className="about btn pointer flex-center">
                    {" "}
                    Download Cv
                  </button>
                </a>
              </div> */}
            </div>
          </div>
        </div>
      </div>
      {/* up to top btn */}
      <div className="up-to-top-btn">
        <a href="#" className="bottom-to-top">
          <i class="fas fa-arrow-up white"></i>
        </a>
      </div>
    </div>
  );
}
